package com.example.vuelafacil20.service;

import com.example.vuelafacil20.dto.ChangePasswordForm;
import com.example.vuelafacil20.model.User;

public interface UserService {

    public Iterable<User>  getAllUsers();
    public User createUser(User user) throws Exception;
    public User getUserById(Integer id) throws Exception;
    public User updateUser(User user) throws Exception;
    public  void deleteUser(Integer id) throws Exception;
    public User changePassword(ChangePasswordForm form) throws Exception;
}

package com.example.vuelafacil20.service;

import com.example.vuelafacil20.model.Role;
import com.example.vuelafacil20.model.User;
import com.example.vuelafacil20.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.expression.ExpressionException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User appUser = userRepository.findByUserName(username).orElseThrow(() -> new UsernameNotFoundException("Login Usuario Invalido."));

        Set<GrantedAuthority> grantList = new HashSet<GrantedAuthority>();
        for (Role role: appUser.getRoles()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getName());
            grantList.add(grantedAuthority);
        }
        UserDetails user = (UserDetails) new org.springframework.security.core.userdetails.User(appUser.getUserName(),appUser.getPassword(),grantList);

        return user;
    }

}
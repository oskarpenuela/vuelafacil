package com.example.vuelafacil20.service;

import com.example.vuelafacil20.dto.ChangePasswordForm;
import com.example.vuelafacil20.model.User;
import com.example.vuelafacil20.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    private boolean checkUsernameAvailable(User user) throws Exception {
        Optional<User> userfound =userRepository.findByUserName(user.getUserName());
        if(userfound.isPresent()){
            throw new Exception("Usuario no disponible");
        }
        return true;
    }

    private boolean checkPasswordValid(User user) throws Exception {
        if(user.getConfirmPassword()==null || user.getConfirmPassword().isEmpty()){
            throw new Exception("Confirmar Contraseña es obligatorio");
        }
        if(!user.getPassword().equals(user.getConfirmPassword())){
            throw new Exception("Contraseñas no coinciden");
        }
        return true;
    }

    @Override
    public User createUser(User user) throws Exception {
        if(checkUsernameAvailable(user) && checkPasswordValid(user)){
            user = userRepository.save(user);
        }
        return user;
    }

    @Override
    public User getUserById(Integer id) throws Exception {
        return userRepository.findById(id).orElseThrow(() -> new Exception("El usuario no existe"));

    }

    @Override
    public User updateUser(User user) throws Exception{
        User foundUser = getUserById(user.getId());
        mapUser(user, foundUser);
        return userRepository.save(foundUser);
    }

    protected void mapUser(User from, User to){
        to.setUserName(from.getUserName());
        to.setName(from.getName());
        to.setLastname(from.getLastname());
        to.setEmail(from.getEmail());
        to.setOffice(from.getOffice());
        to.setRoles(from.getRoles());
    }

    @Override
    @PreAuthorize("hasAnyRole('ADMIN')")
    public void deleteUser(Integer id) throws Exception {
        User user = getUserById(id);
        userRepository.delete(user);
    }

    @Override
    public User changePassword(ChangePasswordForm form) throws Exception {
        User user = getUserById(form.getId());
        if ( !isLoggedUserADMIN() && !user.getPassword().equals(form.getCurrentPassword())) {
                throw new Exception ("Contraseña inválida.");
            }
        if( user.getPassword().equals(form.getNewPassword())) {
           throw new Exception ("La contraseña nueva debe ser diferente a la actual.");
        }
        if( !form.getNewPassword().equals(form.getConfirmPassword())) {
            throw new Exception ("Las contraseñas no coinciden.");
        }

        String encodePassword = bCryptPasswordEncoder.encode(form.getNewPassword());
        user.setPassword(encodePassword);
        return userRepository.save(user);
    }

    private boolean isLoggedUserADMIN() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails loggedUser = null;
        if (principal instanceof UserDetails) {
            loggedUser = (UserDetails) principal;
            loggedUser.getAuthorities().stream()
                    .filter(x -> "ADMIN".equals(x.getAuthority() ))
                    .findFirst().orElse(null); //loggedUser = null;
        }
        return loggedUser != null ?true :false;
    }
}

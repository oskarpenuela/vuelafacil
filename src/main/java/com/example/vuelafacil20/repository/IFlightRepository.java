package com.example.vuelafacil20.repository;

import com.example.vuelafacil20.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IFlightRepository extends JpaRepository <Flight, Integer> {
}

package com.example.vuelafacil3login.persistence.repository;

import com.example.vuelafacil3login.persistence.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRoleRepository extends JpaRepository<Role, Integer> {
}

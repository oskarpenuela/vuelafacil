package com.example.vuelafacil3login.persistence.repository;

import com.example.vuelafacil3login.persistence.entities.Reserve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IReserveRepository extends JpaRepository<Reserve, Integer> {

    @Query("SELECT r FROM Reserve r WHERE r.idPassenger=?1")
    Optional<List<Reserve>> findByIdPassenger(Integer idPassenger);

}

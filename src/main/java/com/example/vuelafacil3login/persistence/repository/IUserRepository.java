package com.example.vuelafacil3login.persistence.repository;

import com.example.vuelafacil3login.persistence.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository <User, Integer>{
}

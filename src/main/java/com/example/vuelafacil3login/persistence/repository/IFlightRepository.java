package com.example.vuelafacil3login.persistence.repository;

import com.example.vuelafacil3login.persistence.entities.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IFlightRepository extends JpaRepository <Flight, Integer> {

    @Query("SELECT f FROM Flight f WHERE f.cityFrom=?1 AND f.cityTo=?2")
    Optional<List<Flight>> searchByCities(String cityFrom, String CityTo);
}

package com.example.vuelafacil3login.persistence.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="reserve")
@Getter
@Setter
public class Reserve implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name="native",strategy = "native")
    private Integer id;
    private Integer idPassenger;
    private String checked;

    @ManyToOne
    @JoinColumn(name = "flight_id",nullable = false)
    private Flight flight;

    @ManyToOne
    @JoinColumn(name = "user_id",nullable = false)
    private User user;

}

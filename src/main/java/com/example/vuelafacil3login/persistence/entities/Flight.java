package com.example.vuelafacil3login.persistence.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="flight")
@Getter
@Setter
public class Flight implements Serializable {

   @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name="native",strategy = "native")
    private Integer id;
    private String cityFrom;
    private String cityTo;
    private LocalDateTime departure;

    @OneToMany(mappedBy = "flight")
    @JsonIgnore
    private Set<Reserve> reserves;

}

package com.example.vuelafacil3login.service;

import com.example.vuelafacil3login.model.FlightDto;
import com.example.vuelafacil3login.persistence.entities.Flight;
import com.example.vuelafacil3login.persistence.repository.IFlightRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FlightService implements IFlightService {

    @Autowired
    IFlightRepository flightRepository;

    @Autowired
    ObjectMapper mapper;

    public void createFlight(FlightDto f){
        Flight flight = mapper.convertValue(f, Flight.class);
        flightRepository.save(flight);
    }

    public Collection<FlightDto> getFlightByFromTo(String cityFrom, String cityTo){
        List<Flight> flights = flightRepository.searchByCities(cityFrom,cityTo).orElse(new ArrayList<>());
        Set<FlightDto> flightsDto = new HashSet<>();
        for(Flight flight: flights){
            FlightDto flightDto = mapper.convertValue(flight, FlightDto.class);
            flightsDto.add(flightDto);
        }
        return flightsDto;
    }

    public void updateFlight(FlightDto f){
        Flight flight = mapper.convertValue(f, Flight.class);
        flightRepository.save(flight);
    }

    public void deleteFlight(Integer id){
        flightRepository.deleteById(id);
    }

    public Collection<FlightDto> getAllFlights() {
        List<Flight> flights = flightRepository.findAll();
        Set<FlightDto> flightsDto = new HashSet<>();
        for (Flight flight : flights) {
            FlightDto flightDto = mapper.convertValue(flight, FlightDto.class);
            flightsDto.add(flightDto);
        }
        return flightsDto;
    }

}

package com.example.vuelafacil3login.service;

import com.example.vuelafacil3login.model.UserDto;
import com.example.vuelafacil3login.persistence.entities.User;
import com.example.vuelafacil3login.persistence.repository.IUserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService implements IUserService {

    @Autowired
    IUserRepository userRepository;

    @Autowired
    ObjectMapper mapper;

    public void createUser(UserDto u){
        User user = mapper.convertValue(u, User.class);

        userRepository.save(user);
    }

    public UserDto getUser(Integer id){
        UserDto userDto = null;
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()){
            userDto = mapper.convertValue(user, UserDto.class);
        }
        return userDto;
    }

    public void updateUser(UserDto u){
        User user = mapper.convertValue(u, User.class);
        userRepository.save(user);
    }

    public void deleteUser(Integer id){
        userRepository.deleteById(id);
    }

    public Collection<UserDto> getAllUsers(){
        List<User> users = userRepository.findAll();
        Set<UserDto> usersDto = new HashSet<>();
        for(User user: users){
            UserDto userDto = mapper.convertValue(user, UserDto.class);
            usersDto.add(userDto);
        }
        return usersDto;
    }

}

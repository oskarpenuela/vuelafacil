package com.example.vuelafacil3login.service;

import com.example.vuelafacil3login.model.ReserveDto;

import java.util.*;

public interface IReserveService {

    public void createReserve(ReserveDto r);

    public Collection<ReserveDto> getReserveByIdPassenger(Integer idPassenger);

    public ReserveDto getReserveById(Integer id);

    public void updateReserve(ReserveDto r);

    public void  deleteReserve(Integer id);

    public Collection<ReserveDto> getAllReserves();
}

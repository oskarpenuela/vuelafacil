package com.example.vuelafacil3login.service;

import com.example.vuelafacil3login.model.UserDto;

import java.util.Collection;

public interface IUserService {

    public void createUser(UserDto u);

    public UserDto getUser(Integer id);

    public void updateUser(UserDto u);

    public void deleteUser(Integer id);

    public Collection<UserDto> getAllUsers();

}

package com.example.vuelafacil3login.service;

import com.example.vuelafacil3login.model.FlightDto;

import java.util.*;

public interface IFlightService {

    public void createFlight(FlightDto f);

    public Collection<FlightDto> getFlightByFromTo(String cityFrom, String cityTo);

    public void updateFlight(FlightDto f);

    public void deleteFlight(Integer id);

    public Collection<FlightDto> getAllFlights();
}

package com.example.vuelafacil3login.service;

import com.example.vuelafacil3login.model.ReserveDto;
import com.example.vuelafacil3login.persistence.entities.Reserve;
import com.example.vuelafacil3login.persistence.repository.IReserveRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ReserveService implements IReserveService{

    @Autowired
    IReserveRepository reserveRepository;

    @Autowired
    ObjectMapper mapper;

    public void createReserve(ReserveDto r){
        Reserve reserve = mapper.convertValue(r, Reserve.class);
        reserveRepository.save(reserve);
    }

    public Collection<ReserveDto> getReserveByIdPassenger(Integer id){
        List<Reserve> reserves = reserveRepository.findByIdPassenger(id).orElse(new ArrayList<>());
        Set<ReserveDto> reservesDto = new HashSet<>();
        for(Reserve reserve: reserves){
            ReserveDto reserveDto = mapper.convertValue(reserve, ReserveDto.class);
            reservesDto.add(reserveDto);
        }
        return reservesDto;
    }

    public ReserveDto getReserveById(Integer id){
        ReserveDto reserveDto = null;
        Optional<Reserve> reserve = reserveRepository.findById(id);
        if(reserve.isPresent()){
            reserveDto = mapper.convertValue(reserve, ReserveDto.class);
        }
        return reserveDto;
    }

    public void updateReserve(ReserveDto r){
        Reserve reserve = mapper.convertValue(r, Reserve.class);
        reserveRepository.save(reserve);
    }

    public void  deleteReserve(Integer id){
        reserveRepository.deleteById(id);
    }

    public Collection<ReserveDto> getAllReserves(){
        List<Reserve> reserves = reserveRepository.findAll();
        Set<ReserveDto> reservesDto = new HashSet<>();
        for(Reserve reserve: reserves){
            ReserveDto reserveDto = mapper.convertValue(reserve, ReserveDto.class);
            reservesDto.add(reserveDto);
        }
        return reservesDto;
    }

}

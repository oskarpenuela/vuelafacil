package com.example.vuelafacil3login.controller;

import com.example.vuelafacil3login.model.ReserveDto;
import com.example.vuelafacil3login.service.IReserveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/reserves")
public class ReserveController {

    @Autowired
    IReserveService reserveService;

    @PostMapping
    public ResponseEntity<?> createR(@RequestBody ReserveDto reserveDto){
        reserveService.createReserve(reserveDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ReserveDto getR(@PathVariable Integer id){
        return reserveService.getReserveById(id);
    }

    @PutMapping()
    public ResponseEntity<?> updateR(@RequestBody ReserveDto reserveDto){
        reserveService.updateReserve(reserveDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteR(@PathVariable Integer id){
        reserveService.deleteReserve(id);
        return ResponseEntity.status(HttpStatus.OK).body("Vuelo eliminado");
    }

    @GetMapping("list")
    public Collection<ReserveDto> getAllR(){
        return reserveService.getAllReserves();
    }
}

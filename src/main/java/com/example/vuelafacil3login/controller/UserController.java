package com.example.vuelafacil3login.controller;

import com.example.vuelafacil3login.model.UserDto;
import com.example.vuelafacil3login.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    IUserService userService;

    @PostMapping
    public ResponseEntity<?> createU(@RequestBody UserDto userDto){
        userService.createUser(userDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public UserDto getU(@PathVariable Integer id){
        return userService.getUser(id);
    }

    @PutMapping()
    public ResponseEntity<?> updateU(@RequestBody UserDto userDto){
        userService.updateUser(userDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteU(@PathVariable Integer id){
        userService.deleteUser(id);
        return ResponseEntity.status(HttpStatus.OK).body("Usuario eliminado");
    }

    @GetMapping("/list")
    public Collection<UserDto> getAllU(){
        return userService.getAllUsers();
    }
}

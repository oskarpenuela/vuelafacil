package com.example.vuelafacil3login.controller;

import com.example.vuelafacil3login.model.FlightDto;
import com.example.vuelafacil3login.service.IFlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/flights")
public class FlightController {

    @Autowired
    IFlightService flightService;

    @PostMapping
    public ResponseEntity<?> createF(@RequestBody FlightDto flightDto){
        flightService.createFlight(flightDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @GetMapping(params = {"cityFrom","cityTo"})
    public Collection<FlightDto> flightsByCities(@RequestParam String cityFrom, @RequestParam String cityTo){
        return flightService.getFlightByFromTo(cityFrom,cityTo);
    }

    @PutMapping()
    public ResponseEntity<?> updateF(@RequestBody FlightDto flightDto){
        flightService.updateFlight(flightDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteF(@PathVariable Integer id){
        flightService.deleteFlight(id);
        return ResponseEntity.status(HttpStatus.OK).body("Vuelo eliminado");
    }

    @GetMapping("/list")
    public Collection<FlightDto> getAllF(){
        return flightService.getAllFlights();
    }

}

package com.example.vuelafacil3login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vuelafacil3Application {
	public static void main(String[] args) {
		SpringApplication.run(Vuelafacil3Application.class, args);
	}
}

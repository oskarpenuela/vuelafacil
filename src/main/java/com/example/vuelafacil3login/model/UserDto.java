package com.example.vuelafacil3login.model;

import com.example.vuelafacil3login.persistence.entities.Role;
import lombok.Data;

import java.util.Set;

@Data
public class UserDto {

    private Integer id;
    private String name;
    private String lastname;
    private String office;
    private String email;
    private String userName;
    private String password;
    private Set<Role> roles;
}

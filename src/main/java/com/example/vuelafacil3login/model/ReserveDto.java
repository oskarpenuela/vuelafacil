package com.example.vuelafacil3login.model;

import com.example.vuelafacil3login.persistence.entities.Flight;
import com.example.vuelafacil3login.persistence.entities.User;
import lombok.Data;

@Data
public class ReserveDto {

    private Integer id;
    private Integer idPassenger;
    private String checked;
    private Flight flight;
    private User user;
}

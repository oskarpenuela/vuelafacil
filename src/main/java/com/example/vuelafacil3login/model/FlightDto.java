package com.example.vuelafacil3login.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class FlightDto {

    private Integer id;
    private String cityFrom;
    private String cityTo;
    private LocalDateTime departure;
}

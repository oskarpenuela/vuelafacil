package com.example.vuelafacil3login.model;

import lombok.Data;

@Data
public class RoleDto {

    private Integer id;
    private String name;

}

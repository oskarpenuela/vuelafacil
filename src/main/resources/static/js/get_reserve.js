window.addEventListener('load', function () {

    (function(){
        event.preventDefault();
        const url = '/reserves/list';
        const settings = {
            method: 'GET'
        }
        fetch(url,settings)
        .then(response => response.json())
        .then(data => {
            for(reserve of data){

                let deleteButton = '<button'+' id=' + '\"' + 'btn_delete_' + reserve.id + '\"' +
//                                             'type="button" onclick="deleteBy('+ reserve.id +')"  class="btn btn-danger btn_delete">' +
                                             ' type="button" onclick="confirmDelete('+reserve.id+')" class="btn btn-info btn_id">' +
                                    "Eliminar"+ '</button>';

                let updateButton = '<button'+' id=' + '\"' + 'btn_id_' + reserve.id + '\"' +
                                             ' type="button" onclick="findBy('+reserve.id+')" class="btn btn-info btn_id">' +

                                    "Modificar" + '</button>';

                let tr_id = 'tr_' + reserve.id;
                let reserveRow = '<tr id=\"' + tr_id + "\"" + '>' +
                        '<td class=\"td_idReserve\">' + reserve.id+ '</td>' +
                        '<td class=\"td_idPassenger\">' + reserve.idPassenger + '</td>' +
                        '<td class=\"td_cityFrom\">' + reserve.flight.cityFrom + '</td>' +
                        '<td class=\"td_cityTo\">' + reserve.flight.cityTo + '</td>' +
                        '<td class=\"td_departure\">' + reserve.flight.departure + '</td>' +
                        '<td>' + deleteButton + '</td>' +
                        '<td>' + updateButton + '</td>' +
                        '</tr>';
                $('#reservesTable tbody').append(reserveRow);
//                console.log(reserve.idPassenger);
            };
        })
    })

    (function(){
      let pathname = window.location.pathname;
      if (pathname == "/reservesList.html") {
          document.querySelector(".nav .nav-item a:last").addClass("active");
      }
    })
})
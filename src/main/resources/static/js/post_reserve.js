window.addEventListener('load', function () {

    const formulario = document.querySelector('#add_new_reserve');

    formulario.addEventListener('submit', function (event) {
        event.preventDefault();
        const form2 = formulario.selectFlights.selectedIndex;
        const formData = {
            idPassenger: parseInt(document.querySelector('#idPassenger').value),
            checked:"PAGADO",
            flight:{id:flightId[form2].value},
            user:{id:1}
        };
        const url = '/reserves';
        const settings = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData)
        }

        fetch(url, settings)
            .then(response => response.json())
            .then(data => {
                 let successAlert = '<div class="alert alert-success alert-dismissible">' +
                     '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                     '<strong></strong> Reserva realizada </div>'
                 document.querySelector('#response').innerHTML = successAlert;
                 document.querySelector('#response').style.display = "block";
            })
            .catch(error => {
                    let errorAlert = '<div class="alert alert-danger alert-dismissible">' +
                                     '<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                     '<strong> Error intente nuevamente</strong> </div>'
                      document.querySelector('#response').innerHTML = errorAlert;
                      document.querySelector('#response').style.display = "block";
            })
    });

    (function(){
        let pathname = window.location.pathname;
        if(pathname === "/"){
            document.querySelector(".nav .nav-item a:first").addClass("active");
        } else if (pathname == "/reserves-list.html") {
            document.querySelector(".nav .nav-item a:last").addClass("active");
        }
    })();
});
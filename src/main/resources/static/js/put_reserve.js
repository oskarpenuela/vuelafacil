window.addEventListener('load', function () {
    const formulario = document.querySelector('#update_reserves_form');
    formulario.addEventListener('submit', function (event) {
        event.preventDefault();

        const form2 = formulario.selectFlights.selectedIndex;
        const form3= document.querySelector('#id_reserve_ini');
        console.log(form3.value);
        const formData = {
            id: form3.value,
            idPassenger: parseInt(document.querySelector('#id_passenger').value),
            checked:"PAGADO",
            flight:{id:flightId[form2].value},
            user:{id:1}
        };
        console.log(formData);
        const url = '/reserves';
        const settings = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData)
        };
        fetch(url,settings)
        .then(response => response.json())
        .then(() => alert("Se actualizó la reserva"))
        .catch(() => alert("No se pudo actualizar la reserva"))
        .finally(() => {
          window.location.href = '/reserves-list.html';
        })
    })
 })

 function findBy(id) {
    const url = '/reserves'+"/"+id;
    const settings = {
        method: 'GET'
    }
    fetch(url,settings)
    .then(response => response.json())
    .then(data => {
      var text = document.createTextNode(data.idPassenger);
      var text1 = document.createTextNode(" ORIGEN: " +data.flight.cityFrom+ "    DESTINO: " +data.flight.cityTo+ "    FECHA: "+ data.flight.departure);
      document.getElementById("id_reserve_ini").appendChild(document.createTextNode(data.id));
      document.getElementById("id_passenger_ini").appendChild(text);
      document.getElementById("id_departure_ini").appendChild(text1);
      document.querySelector('#id_passenger').value = data.idPassenger;
      document.querySelector('#id_reserve_ini').value = data.id;
      document.querySelector('#div_reserve_table').style.display = "none";
      document.querySelector('#div_reserves_updating').style.display = "block";
    }).catch(error => {
        alert("Error: " + error);
    })
}
window.addEventListener('load', function () {

    (function(){
        event.preventDefault();
        const url = '/users/list';
        const settings = {
            method: 'GET'
        }
        fetch(url,settings)
        .then(response => response.json())
        .then(data => {
            for(user of data){

                let deleteButton = '<button' +
                                          ' id=' + '\"' + 'btn_delete_' + user.id + '\"' +
                                          ' type="button" onclick="deleteBy('+user.id+')" class="btn btn-danger btn_delete">' +
                                          "Eliminar"+
                                          '</button>';

                let updateButton = '<button' +
                                          ' id=' + '\"' + 'btn_id_' + user.id + '\"' +
                                          ' type="button" onclick="findBy('+user.id+')" class="btn btn-info btn_id">' +
                                          "Modificar" +
                                          '</button>';

                let tr_id = 'tr_' + user.id;
                let userRow = '<tr id=\"' + tr_id + "\"" + '>' +
                        '<td class=\"td_name\">' + user.name + '</td>' +
                        '<td class=\"td_lastname\">' + user.lastname + '</td>' +
                        '<td class=\"td_office\">' + user.office + '</td>' +
                        '<td class=\"td_email\">' + user.email + '</td>' +
                        '<td class=\"td_userName\">' + user.userName + '</td>' +
                        '<td class=\"td_roles\">' + user.roles[0].name + '</td>' +
                        '<td>' + deleteButton + '</td>' +
                        '<td>' + updateButton + '</td>' +
                        '</tr>';
                $('#usersTable tbody').append(userRow);
                console.log(user);
            };
        })
    })

    (function(){
      let pathname = window.location.pathname;
      if (pathname == "/users-list.html") {
          document.querySelector(".nav .nav-item a:last").addClass("active");
      }
    })
})
function confirmDelete(id)
{
    const message = "";
    const option = confirm("Desea eliminar la reserva ?");
    if(option == true){
          const url = '/reserves/'+ id;
          const settings = {
              method: 'DELETE'
          }
          fetch(url,settings)
          .then(response => response.json())

          let row_id = "#tr_" + id;
          document.querySelector(row_id).remove();
          alert( "La reserva se ha eliminado correctamente" );

    }else{
        alert( "La reserva no ha sido eliminada" );
    }
}


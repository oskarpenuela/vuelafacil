window.addEventListener('load', function () {

    (function(){
        event.preventDefault();
        const url = '/flights/list';
        const settings = {
            method: 'GET'
        }
        fetch(url,settings)
        .then(response => response.json())
        .then(data => {
            for(flight of data){
                let deleteButton = '<button' +
                                          ' id=' + '\"' + 'btn_delete_' + flight.id + '\"' +
                                          ' type="button" onclick="deleteBy('+flight.id+')" class="btn btn-danger btn_delete">' +
                                          "Eliminar"+
                                          '</button>';

                let updateButton = '<button' +
                                          ' id=' + '\"' + 'btn_id_' + flight.id + '\"' +
                                          ' type="button" onclick="findBy('+flight.id+')" class="btn btn-info btn_id">' +
                                          "Modificar" +
                                          '</button>';

                let tr_id = 'tr_' + flight.id;
                let flightRow = '<tr id=\"' + tr_id + "\"" + '>' +
                        '<td class=\"td_cityFrom\">' + flight.cityFrom + '</td>' +
                        '<td class=\"td_cityTo\">' + flight.cityTo + '</td>' +
                        '<td class=\"td_departure\">' + flight.departure + '</td>' +
                        '<td>' + deleteButton + '</td>' +
                        '<td>' + updateButton + '</td>' +
                        '</tr>';
                $('#flightsTable tbody').append(flightRow);
            };
        })
    })

    (function(){
      let pathname = window.location.pathname;
      if (pathname == "/flights-list.html") {
          document.querySelector(".nav .nav-item a:last").addClass("active");
      }
    })
})